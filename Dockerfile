FROM alpine:latest


RUN apk --update add --no-cache python py-pip groff less bash curl git unzip && \
    pip install -U awscli && \
    apk --purge -v del py-pip && \
    rm -rf `find / -regex '.*\.py[co]'`

RUN adduser -D user
COPY test /
ENTRYPOINT ["/bin/sh"]
USER user
